# test.conj.region

<!-- badges: start -->
<!-- badges: end -->

<!-- Avant de commencer à rédiger le contenu -->
<!-- Créer un projet sur GitLab **sans** README --> 
<!-- Créer une variable 'PROJECT_ACCESS_TOKEN' sur GitLab dans votre projet en suivant les instructions sur :-->
<!-- https://github.com/statnmap/GitLab-Pages-Deploy#publish-one-site-for-each-branch -->

Ce livre numérique contient ...

## Organisation du projet

Le fonctionnement des contributions est expliquée dans
<CONTRIBUTING.md>

## Génération automatique des documents

Les documents sont générés automatiquement grâce à un dispositif qui s'appelle
l'intégration continue. La dernière version de ces documents est disponible en
cliquant ici :
*<url-pages/main>*

## Un rapport pour chaque branche importante

Le rapport est compilé et généré automatiquement dès fusion vers la branche principale (`main`). La dernière version est accessible ici: <url-pages/main>
Cette version peut-être imprimée en PDF en l'état.

Chaque branche spécifique comme `production` ou `validation` montre une version du rapport dans cette branche :

- Pour `production`: <url-pages/production>
- Pour `validation`: <url-pages/validation>
Toutes les versions de branches spécifiques sont visibles dans l'index: <url-pages/index.html>

_Pour changer l'apparence de la sortie HTML (couleurs, espaces, ...), il est nécessaire de modifier les ressources du package {conj.region}._

## Un rapport au format odt à jour

Le rapport au format "odt" peut être téléchargé dans sa dernière version ici: <url-pages/main/rapport.odt>

_Note : Pour ce format, vous avez la possibilité de changer le template par défaut inclu dans le 'pandoc/reference.odt'_

## Un dossier avec toutes les fiches dans des odt séparés

Toutes les fiches au format odt sont téléchargeables dans un dossier compresssé ici: <url-pages/main/odt_files.zip>

## Rédiger le rapport en local et envoyer ses modifications

Voir ["CONTRIBUTING.md"](CONTRIBUTING.md)

### Détails techniques

La conversion s'effectue grâce à R et [pandoc](https://pandoc.org).  
L'intégration continue utilise l'image docker officielle de rocker qui est une image Ubuntu avec pandoc d'installé.

## Configuration de git sur les postes de travail

*Dans le Terminal*  
``` bash
git config --global user.name "Toto Tata"
git config --global user.email "toto@insee.fr"

# Pour un projet sur gitlab.insee.fr
git config --global http.https://gitlab.insee.fr.proxy ""
git config --global http.https://gitlab.insee.fr.sslverify "false"

# Pour un projet sur ssp.cloud, trouver le proxy pour sortir et définir
git config --global http.https://git.lab.sspcloud.fr.proxy "http://mon.proxy.pour.sortir.fr"
git config --global http.https://git.lab.sspcloud.fr.sslverify "false"
```

## Utilisation du mode Editeur Visuel dans RStudio

> Il est recommandé de faire en sorte qu'il n'y ait qu'une phrase par ligne pour faciliter le suivi des modifications dans git. Si vous utilisez l'Editeur Visuel, activez cette fonctionnalité dans RStudio : Tools > Global Options > RMarkdown > Visual > "Automatic Text Wrapping (Line break) : 'sentence'"

## Modifications du YAML de "index.Rmd"

-   `title` : le titre de la note de conjoncture régionale
-   `region` : le nom de la région éditrice
-   `date` : la date de la note au format "Mois 20xx" (ex: "Mai 2022")
-   `numero` : le numéro de la note (ex: "29")
-   `adresse` : les coordonnées de l'Insee de la région concernée (utiliser "</br>" pour indiquer un retour à la ligne)
-   `direction-publication` : le nom du/de la directeur.ice de publication
-   `redaction-en-chef` : le nom du/de la rédacteur.ice en chef
-   `telephone` : le numéro de téléphone du bureau de presse
-   `issn` : le numéro ISSN de la note

## Modifications du YAML de rendu dans "\_output.yml"

Les options de `conj.region::paged_conj()` permettent de choisir :

-   `front_logo` : chemin local ou URL vers l'image du logo à utiliser en première page
-   `band_image` : chemin local ou URL vers l'image du bandeau à utiliser en première page
-   `qr_code` : chemin local ou URL vers l'image du QR code à utiliser en dernière page
-   `back_logo` : chemin local ou URL vers l'image du logo à utiliser en dernière page
-   `other_css` : chemin local vers un fichier css complémentaire

Vous pouvez aussi utiliser les options de `bookdown::html_document2()` :

- `number_sections` : Faut-il numéroter les sections (`true`) ou non (`false`).
    + Notez que vous pouvez utiliser la notation `## Mon titre {-}` pour que ce titre ne soit pas numéroté.

## Besoin d'aide ?

Travail collaboratif avec la méthode PROPRE: <https://rdes_dreal.gitlab.io/publication_guide/>

Travail collaboratif avec R :  
Utiliser Git avec R : <https://linogaliana.gitlab.io/collaboratif/git.html>  
Bonnes pratiques de Git et Gitlab :
<https://linogaliana.gitlab.io/collaboratif/gitexo.html>

utilitR :  
Configurer Git sur son poste de travail :
<https://www.book.utilitr.org/git-config.html>  
Utiliser Git avec R studio : <https://www.book.utilitr.org/git.html>

Présentation de Git (diapo) : <https://www.overleaf.com/read/zngrwqbkvznw>

Posez votre question sur le canal \#digitalisation du slack de la Conjoncture !

# Rappel pour la relecture

## Relecture et ré-écriture sur GitLab

- Choisir la branche "Validation" dans le menu déroulant de la page d'accueil
- Cliquer sur le bouton "Web IDE"
- Apporter ses modifications
- Aller dans le menu de "Commit"
- Ecrire un message de _commit_ indiquant vos principales modifications
- Cliquer sur le bouton _commit_ (dans la branche validation directement)
- Attendre quelques minutes que l'intégration continue compile votre note
- S'assurer que la compilation a fonctionné
- Aller voir sa version compilée: <url-pages/validation>

## Relecture en mode commentaires sur GitLab

-   Aller dans `Merge Requests` dans le panneau de gauche
-   Cliquer sur la merge request de _validation_ vers _main_ : "Draft: Validation"
-   Aller dans l'onglet `Changes`
-   *Pro tip* : pour réduire les commentaires déjà existants pour ne pas
    perturber la lecture, cliquer sur "`Toggle Comments`" en haut à droite du
    fichier que vous relisez
-   Sélectionner la fiche à relire, il est possible d'en ouvrir une version mise
    en page en cliquant sur `view file` à droite du nom de la fiche. Pour
    modifier ou faire une remarque, le processus est alors le suivant :
-   Sur la ligne à modifier, cliquer dans la marge à gauche sur
    `Add a comment to this line`
-   Pour faire une remarque, il suffit d'écrire, et pour suggérer une
    modification, il est possible d'utiliser le bouton `Insert Suggestion`.
    Modifier les numéros des balises si on veut modifier plusieurs lignes.
-   Valider son commentaire (`Add Comment Now`). Les rédacteurs de la fiche
    répondront au commentaire à cet endroit, ou accepteront directement les
    suggestions.
